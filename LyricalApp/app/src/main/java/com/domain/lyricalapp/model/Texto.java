package com.domain.lyricalapp.model;

public class Texto {

    private String titulo;
    private String txt;

    public Texto(String titulo,String texto){
        this.titulo = titulo;
        this.txt = texto;

    }

    public String getTitulo() {
        return titulo;
    }

    public String getTxt() {
        return txt;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setTxt(String txt){
        this.txt = txt;
    }
}
