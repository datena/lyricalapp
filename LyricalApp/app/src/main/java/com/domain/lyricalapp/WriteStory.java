package com.domain.lyricalapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WriteStory extends AppCompatActivity {


    private EditText Title;
    private EditText texto;
    private Button Submit;
    private Button cancel;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private AlertDialog alerta;
    private ArrayList <String> arrayList;
    private ArrayAdapter <String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.write_story);
        mAuth = FirebaseAuth.getInstance();

        Title = (EditText) findViewById(R.id.wsTitleTxt);
        texto = (EditText) findViewById(R.id.wsParagraphTxt);
        Submit = (Button) findViewById(R.id.wsSubmitButt);
        cancel = (Button) findViewById(R.id.wsBackButt);



        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth){
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.d("AUTH", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    Log.d("AUTH", "onAuthStateChanged:signed_out");
                }
            }
        };
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String titulo = Title.getText().toString();
                String text = texto.getText().toString();
                if (text.matches("")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(WriteStory.this);
                    builder.setMessage("Texto deixado em branco");
                    builder.setTitle("Erro");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    alerta = builder.create();
                    alerta.show();
                }
                if (titulo.matches("")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(WriteStory.this);
                    builder.setMessage("Titulo deixado em branco");
                    builder.setTitle("Erro");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    alerta = builder.create();
                    alerta.show();
                }
                Bundle bundle = new Bundle();
                bundle.putString("titulo",titulo);
                bundle.putString("texto",text);
                Intent p = new Intent(WriteStory.this,Feed.class);
                p.putExtras(bundle);
                startActivity(p);
            }
        });

    }


}// @end
