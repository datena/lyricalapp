package com.domain.lyricalapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class localtroca extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private Button BtnSenhaNovo;
    private EditText EditEmailNovo;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.localtroca);
        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth){
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.d("AUTH", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    Log.d("AUTH", "onAuthStateChanged:signed_out");
                }
            }
        };

        EditEmailNovo = (EditText) findViewById(R.id.editEmailSend);

        BtnSenhaNovo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                String p = EditEmailNovo.getText().toString().trim();
                if (TextUtils.isEmpty(p)){
                    android.widget.Toast.makeText(getApplication(), "Por favor informe seu email, para modificar a senha", Toast.LENGTH_SHORT).show();
                }else{
                    mAuth.sendPasswordResetEmail(p).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()){
                                    android.widget.Toast.makeText(getApplicationContext(),"Porfavor Verifique seu email do gmail, se você quiser redefinie sua senha",Toast.LENGTH_LONG).show();
                                    startActivity(new Intent(localtroca.this,Login.class));
                                }else {
                                    String message = task.getException().getMessage().toString();
                                    Toast.makeText(getApplicationContext(),"Ocorreu um erro:" +  message ,Toast.LENGTH_LONG).show();
                                }
                        }
                    });
                }
            }
        });
    }
}
