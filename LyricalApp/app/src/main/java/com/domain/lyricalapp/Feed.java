package com.domain.lyricalapp;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Feed extends AppCompatActivity {

    //private RecyclerView recyclerView;
    private TextView txtBemVindo;
    private Toolbar toolbarLyrical;
    private Button Logout;
    private Button Write;
    private Button one;
    private Button two;
    private Button three;
    private ArrayList<String> arrayList;
    private ArrayAdapter<String> adapter;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private SQLiteDatabase bancodedados;
    private ListView accStoriesListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account);
        mAuth = FirebaseAuth.getInstance();

        Logout = (Button) findViewById(R.id.accLogoutButt);
        Write = (Button) findViewById(R.id.accWriteButt);
        one = (Button) findViewById(R.id.tab_one);
        two = (Button) findViewById(R.id.tab_two);
        three = (Button) findViewById(R.id.tab_three);

        ListView accStoriesListView = (ListView) findViewById(R.id.accStoriesListView);


        Intent intencao = getIntent();
        Bundle pacote = intencao.getExtras();

        String titulo2 = pacote.getString("titulo");
        String texto2 = pacote.getString("texto");

        String titulo3 = titulo2.toString();
        String texto3 = texto2.toString();

        String[] item = {"Fabula",titulo3};
        String[] item2 = {"Aqui vai falar sobre uma fabula",texto3};

        ArrayList<HashMap<String, String>> valores= new ArrayList<>();
        for (int i = 0; i <item.length;i++){
                HashMap<String, String> itemj = new HashMap<>();
                itemj.put("titulo",item[i]);
                itemj.put("texto",item2[i]);
                valores.add(itemj);
        }

        String chaves[] = {"titulo","texto"};
        int[] labels = {R.id.txtTituloList,R.id.txtTextoList};

        SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(),valores,R.layout.item,chaves,labels);

        Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                Intent opo = new Intent(Feed.this,Intro.class);
                startActivity(opo);
            }
        });

        Write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent opu = new Intent(Feed.this,WriteStory.class);
                startActivity(opu);
            }
        });

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth){
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.d("AUTH", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    Log.d("AUTH", "onAuthStateChanged:signed_out");
                }
            }
        };



    }

    class CustomAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return 0;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return null;
        }
    }

}
