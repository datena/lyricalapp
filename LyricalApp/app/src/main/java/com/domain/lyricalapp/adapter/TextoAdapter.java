package com.domain.lyricalapp.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.domain.lyricalapp.model.Texto;

import java.util.List;

public class TextoAdapter extends BaseAdapter {

    private Activity atividade;
    private List<Texto> lista;

    public TextoAdapter(Activity atividade, List<Texto> lista){
            this.atividade = atividade;
            this.lista = lista;
    }

    @Override
    public int getCount(){
        return lista.size();
    }

    @Override
    public Object getItem(int position){
        return lista.get(position);
    }

    @Override
    public long getItemId(int position){
            return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }
}
